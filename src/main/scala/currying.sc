def partial[A, B ,C](a: A, f: (A, B) => C) : B => C = {
  b => f(a, b)
}

def curry[A, B, C](f: (A, B) => C): A => (B => C) = {
  a: A => b: B => f(a, b)
}

def uncurry[A, B, C](f: A => B => C): (A, B) => C = {
  (a: A, b: B) => f(a)(b)
}

def compose[A, B, C](f: B => C, g: A => B) : A => C = {
  x => f(g(x))
}

def add(x:Int, y:Int): Int = x + y

assert(
  partial[Int, Int, Int](5, add)(11) == add(5, 11),
  "Partial test failed"
)

assert(
  curry(add)(4)(6) == add(4, 6)
)

assert(
  uncurry(curry(add))(4, 6) == add(4, 6)
)

curry(add)
curry(add)(4)
curry(add)(4)(6)

uncurry(curry(add))(4, 5)

compose(curry(add)(2), curry(add)(3))(1)