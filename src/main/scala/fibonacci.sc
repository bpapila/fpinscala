object Fibonacci {

  import scala.collection.parallel.immutable

  def fib(n: Int): Int = {
    @annotation.tailrec
    def go(n: Int, prev: Int, cur: Int): Int = {
      if (n == 0) 0
      else if (n == 1) cur
      else go(n - 1, cur, prev + cur)
    }

    go(n, 0, 1)
  }

  // test
  // fib(0) => 0
  assert(fib(0) == 0)
  // fib(1) => 1
  assert(fib(1) == 1)
  // fib(2) => 1
  assert(fib(2) == 1)
  // fib(3) => 2
  assert(fib(3) == 2)
  // fib(4) => 2
  assert(fib(4) == 3)
  // fib(5) => 5
  assert(fib(5) == 5)
  // fib(6) => 8
  assert(fib(0) == 0)
  // fib(20) => 6765
  assert(fib(20) == 6765)
}


