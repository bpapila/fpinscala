package fputil

sealed trait List[+A]
case object Nil extends List[Nothing]
case class Cons[A](h: A, t: List[A]) extends List[A]

object List {

  def apply[A](a: A*): List[A] = {
    if (a.isEmpty) Nil
    else Cons(a.head, apply(a.tail: _*))
  }

  def headOption[A](l: List[A]): Option[A] = {
    l match {
      case Cons(h, _) => Some(h)
      case _ => None
    }
  }

  def tail[A](l: List[A]): List[A] = {
    l match {
      case Cons(_, t) => t
      case _ => Nil
    }
  }

  def setHead[A](a: A, l: List[A]): List[A] = {
    l match {
      case Nil => Cons(a, Nil)
      case Cons(_, t) => Cons(a, t)
    }
  }

  def drop[A](l: List[A], n: Int): List[A] = {
    l match {
      case Nil => Nil
      case Cons(_, t) if n > 0 => drop(t, n - 1)
      case _ => l
    }
  }

  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = {
    l match {
      case Nil => Nil
      case Cons(h, t) if f(h) => dropWhile(t, f)
      case _ => l
    }
  }

  def init[A](l: List[A]): List[A] = {
    l match {
      case Nil => Nil
      case Cons(_, Nil) => Nil
      case Cons(h, t) => Cons(h, init(t))
    }
  }

  def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B =
    foldLeft(reverse(as), z)((x, y) => f(y, x))

  // scala.deprecated
  def foldRightDeprecated[A, B](as: List[A], z: B)(f: (A, B) => B): B =
    as match {
      case Nil => z
      case Cons(h, t) => f(h, foldRightDeprecated(t, z)(f))
    }

  def foldLeft[A, B](as: List[A], z: B)(f: (B, A) => B): B =
    as match {
      case Nil => z
      case Cons(h, t) => foldLeft(t, f(z, h))(f)
    }

  def length[A](l: List[A]): Int = foldLeft(l, 0)((y, _) => 1 + y)

  def reverse[A](l: List[A]): List[A] = foldLeft(l, Nil: List[A])((x, y) => Cons(y, x))

  def append[A](a1: List[A], a2: List[A]): List[A] = foldRight(a1, a2)((x, y) => Cons(x, y))

  def flatten[A](l: List[List[A]]): List[A] = foldRight(l, Nil: List[A])((z, h) => append(z, h))

  def map[A, B](l: List[A])(f: A => B): List[B] =
    foldRight[A, List[B]](l, Nil: List[B])((h, acc) => Cons(f(h), acc))

  def filter2[A](l: List[A])(f: A => Boolean): List[A] =
    foldRight[A, List[A]](l, Nil: List[A])((h, acc) => if(f(h)) Cons(h, acc) else acc)

  def filter[A](l: List[A])(f: A => Boolean): List[A] =
    flatMap(l)(a => if(f(a)) Cons(a, Nil) else Nil)

  def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] =
    foldRight[A, List[B]](l, Nil: List[B])((h, acc) => append(f(h), acc))

  def addLists(l1: List[Int], l2: List[Int]): List[Int] = {
    (l1, l2) match {
      case (Cons(h, t), Cons(h2, t2)) => Cons(h + h2, addLists(t, t2))
      case (Nil, Cons(_, _))  => Nil
      case (Cons(_, _), Nil)  => Nil
      case (Nil, Nil)         => Nil
    }
  }

  def zipWith[A](l1: List[A], l2: List[A])(f: (A, A) => A): List[A] = {
    (l1, l2) match {
      case (Cons(h, t), Cons(h2, t2)) => Cons(f(h, h2), zipWith(t, t2)(f))
      case (Nil, Cons(_, _))  => Nil
      case (Cons(_, _), Nil)  => Nil
      case (Nil, Nil)         => Nil
    }
  }



}
