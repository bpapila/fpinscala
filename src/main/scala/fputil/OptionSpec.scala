package fputil

object OptionSpec {

  def main(args: Array[String]): Unit = {

    val maybeInt: Option[Int] = Some(5)

    println(Some(0).map[Double](_+1))
    println(maybeInt.flatMap(safeDivisonByItself))

    println(maybeInt.getOrElse(4))
    println(Some(0).flatMap(safeDivisonByItself).getOrElse(1))
    println(Some(0).flatMap(safeDivisonByItself).orElse[Double](Some(100.0)))

    println(Some(0).filter(_ > 2))
    println(Some(3).filter(_ > 2))
  }

  def safeDivisonByItself(n: Int): Option[Double] = {
    if (n == 0)
      None
    else
      Some(n / n)
  }
}
