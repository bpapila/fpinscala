package fputil

sealed trait Tree[+A]
case class Leaf[A](v: A) extends Tree[A]
case class Branch[A](l: Tree[A], r: Tree[A]) extends Tree[A]

object Tree {

  def size[A](t: Tree[A]): Int = t match {
    case Branch(l, r) => size(l) + size(r)
    case Leaf(_) => 1
  }

  def max(t: Tree[Int]): Int = t match {
    case Branch(l, r) => Integer.max(max(l), max(r))
    case Leaf(v) => v
  }

}
