object Monomorphic {

  def findFirstString(arr: Array[String], str: String): Int = {
    @annotation.tailrec
    def go(n: Int): Int = {
      if (n >= arr.length) -1
      else if (arr(n) == str) n
      else go(n + 1)
    }

    go(0)
  }

  assert(
    findFirstString(Array("a", "b"), "a") == 0,
    "not 1"
  )
  assert(
    findFirstString(Array("a", "b"), "b") == 1
  )
  assert(
    findFirstString(Array("a", "b"), "c") == -1
  )
  assert(
    findFirstString(Array(), "c") == -1
  )

}

object Polymorphic {
  def findFirst[A](arr: Array[A], f: A => Boolean): Int = {
    @annotation.tailrec
    def go(n: Int): Int = {
      if (n >= arr.length) -1
      else if (f(arr(n))) n
      else go(n+1)
    }

    go(0)
  }

  def testA = (x: String) => x == "a"

  assert(
    findFirst[String](Array("a", "b"), _ == "a") == 0,
    1
  )
  assert(
    findFirst[String](Array("b"), _ == "a") == -1,
  )

  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
    @annotation.tailrec
    def go(n: Int): Boolean = {
      if (n >= as.length - 1) true
      else if (!ordered(as(n), as(n+1))) false
      else go(n + 1)
    }

    go(0)
  }

  

  assert(
    isSorted[Int](Array(1, 2, 3), (x, y) => x < y) == true, "1,2,3 sorted test failed"
  )

  assert(
    isSorted[Int](Array(2, 3, 1), (x, y) => x < y) == false, "2, 3, 1 sorted test failed"
  )

  assert(
    isSorted[Int](Array(), (x, y) => x < y) == true, "empty list sorted test failed"
  )

  assert(
    isSorted[String](Array("1", "2", "3"), (x, y) => x < y) == true, "1,2,3 sorted test failed"
  )

  assert(
    isSorted[String](Array("2", "3", "1"), (x, y) => x < y) == false, "2, 3, 1 sorted test failed"
  )

  assert(
    isSorted[String](Array(), (x, y) => x < y) == true, "empty list sorted test failed"
  )

}

Monomorphic
Polymorphic

