package fputil

import org.scalatest.FunSpec

class ListTest extends FunSpec {

  import List._

  describe("A set") {

    describe("When empty") {
      describe("tail") {
        it("returns Nil") {
          assert(List() == Nil)
        }
      }
      describe("setHead") {
        it("returns a new list with set value") {
          assert(setHead(5, Nil) == List(5))
        }
      }

      describe("drop") {
        it("returns Nil") {
          assert(drop(Nil, 0) == Nil)
          assert(drop(Nil, 1) == Nil)
          assert(drop(Nil, 1000) == Nil)
          assert(drop(Nil, -1) == Nil)
        }

      }

      describe("dropWhile") {
        it("returns Nil") {
          assert(dropWhile(Nil, even) == Nil)
          assert(dropWhile(Nil, even) == Nil)
          assert(dropWhile(Nil, even) == Nil)
        }
      }

      describe("init") {
        it("returns nil") {
          assert(init(Nil) == Nil)
        }
      }

      describe("foldRight") {
        it("returns sum") {
          assert(foldRight(Nil, 0)(sum) == 0)
          assert(foldRight(Nil, 1)(sum) == 1)
          assert(foldRight(Nil, -1)(sum) == -1)
        }
      }

      describe("foldLeft") {
        it("returns sum") {
          assert(foldLeft(Nil, 0)(sum) == 0)
          assert(foldLeft(Nil, -1)(sum) == -1)
        }
      }

      describe("reverse") {
        it("returns nil") {
          assert(reverse(Nil) == Nil)
        }
      }

      describe("append") {
        it("returns nil") {
          assert(reverse(Nil) == Nil)
        }

        it("returns the list to add") {
          assert(append(Nil, List(1)) == List(1))
        }
      }

      describe("flatten") {
        it("returns nil") {
          assert(flatten(Nil) == Nil)
        }
      }

      describe("map") {
        it("returns nil") {
          assert(map(Nil: List[Int])(n => n.toString) == Nil)
        }
      }

      describe("filter") {
        it("returns nil") {
          assert(filter(Nil: List[Int])(even) == Nil)
        }
      }

      describe("flatMap") {
        it("returns Nil") {
          assert(flatMap(Nil: List[Int])(n => List(n, n)) == Nil)
        }
      }

      describe("addLists") {
        it("returns Nil") {
          assert(addLists(Nil: List[Int], Cons(1, Nil)) == Nil)
        }
      }
    }

    // ***** //

    describe("tail") {
      it("returns tail") {
        assert(tail(List(1, 2)) == List(2))
      }
    }

    describe("setHead") {
      it("creates a new list replacing the head") {
        assert(setHead(5, List(1, 2)) == List(5, 2))
      }
    }

    describe("drop") {
      it("drops x elements from list") {
        assert(drop(List(1), 1) == Nil)
        assert(drop(List(1, 2, 3), 1) == List(2, 3))
        assert(drop(List(1, 2, 3), 3) == Nil)
        assert(drop(List(1, 2, 3), 30) == Nil)
        assert(drop(List(1), -1) == List(1))
      }
    }

    describe("dropWhile") {
      it("drops while even") {
        assert(dropWhile(List(1), even) == List(1))
        assert(dropWhile(List(2), even) == Nil)
        assert(dropWhile(List(0), even) == Nil)
        assert(dropWhile(List(2, 4, 5), even) == List(5))
        assert(dropWhile(List(1, 2, 3), even) == List(1, 2, 3))
      }

    }

    describe("init") {
      it("returns nil") {
        assert(init(List(1)) == Nil)
        assert(init(List(1, 2)) == List(1))
      }
    }

    describe("foldRight") {
      it("returns sum of values") {
        assert(foldRight(List(1), 0)(sum) == 1)
        assert(foldRight(List(1), 1)(sum) == 2)
        assert(foldRight(List(-1), 0)(sum) == -1)
        assert(foldRight(List(1, 2), 0)(sum) == 3)
      }

      it("returns a copy of the list") {
        assert(foldRight(List(1, 2), Nil: List[Int])(Cons(_, _)) == List(1, 2))
      }

      it("returns concatted string") {
        assert(foldRight(List(1, 2), "")((h, z) => concat(h.toString, z)) == "12")
        assert(foldRight(List(1, 2), "end")((h, z) => concat(h.toString, z)) == "12end")
      }

      it("applies f in order from left to right zero at the end") {
        assert(foldRight(List(4, 2), 1)((h, z) => h / z) == 2)
      }
    }

    describe("length") {
      it("counts length") {
        assert(length(Nil) == 0)
        assert(length(List(1, 99)) == 2)
      }
    }

    describe("foldLeft") {
      it("returns sum") {
        assert(foldLeft(List(1), 0)(sum) == 1)
        assert(foldLeft(List(1, 2), 0)(sum) == 3)
        assert(foldLeft(List(1, 2), 5)(sum) == 8)
      }

      it("returns concatted string") {
        assert(foldLeft(List(1, 2), "")((z, h) => concat(z, h.toString)) == "12")
        assert(foldLeft(List(1, 2), "end")((z, h) => concat(z, h.toString)) == "end12")
      }

      it("applies f in order from right to left zero at the start") {
        assert(foldLeft(List(2, 2), 4)((h, z) => h / z) == 1)
      }

      it("concats") {
        assert(foldLeft(List("a", "b", "c"), "")(concat) == "abc")
      }
    }

    describe("reverse") {
      it("returns reverse of list") {
        assert(reverse(List(1, 2, 3)) == List(3, 2, 1))
      }
    }

    describe("append") {
      it("appends l1 by l2") {
        assert(append(List(1), List(2)) == List(1, 2))
      }
    }

    describe("flatten") {
      it("returns flattens a list") {
        assert(flatten(List(List(1, 2), List(4, 5))) == List(1, 2, 4, 5))
        assert(flatten(List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))) == List(1, 2, 3, 4, 5, 6, 7, 8, 9))
      }
    }

    describe("map") {
      it("return a list of strings") {
        assert(map(List(1,2,3))(n => n.toString) == List("1", "2", "3"))
      }

      it("doubles the elements") {
        assert(map(List(1,2,3))(n => n * 2) == List(2, 4, 6))
      }
    }

    describe("filter") {
      it("returns evens") {
        assert(filter(List(1, 2, 3, 4))(even) == List(2, 4))
      }
    }


    describe("flatMap") {
      it("returns a flatten list") {
        assert(flatMap(List(1, 2, 3))(n => List(n, n)) == List(1, 1, 2, 2, 3, 3))
      }
    }

    describe("zipWith") {
      it("adds lists' elements") {
        assert(zipWith(Cons(1, Nil), Cons(1, Nil))((x,y) => x+y) == Cons(2, Nil))
      }
    }

  }

  def even(n: Int): Boolean = n % 2 == 0
  def sum(x: Int, y: Int): Int = x + y
  def concat(a: String, b: String): String = s"$a$b"

}
