package fputil

import org.scalatest.FunSpec

class TreeTest extends FunSpec {

  describe("size") {

    import Tree._

    it("should size") {
        assert(size(
          Branch(Leaf(1), Leaf(123))
        ) == 2)

      assert(size(
        Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Branch(Leaf(5), Leaf(6))))
      ) == 5)
    }

    it("should find max") {
      assert(max(
        Branch(Leaf(1), Leaf(123))
      ) == 123)

      assert(max(
        Branch(Branch(Leaf(9), Leaf(2)), Branch(Leaf(333), Branch(Leaf(5), Leaf(6))))
      ) == 333)

      assert(max(
        Branch(Branch(Leaf(9), Leaf(2)), Branch(Leaf(3), Branch(Leaf(5), Leaf(6))))
      ) == 9)
    }

  }
}
